﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyekAkhir_Teknet.Models;
using System.Data.SqlClient;

namespace ProyekAkhir_Teknet.Controllers
{
    public class AkunController : Controller
    {

        SqlConnection con = new SqlConnection();
        SqlCommand com = new SqlCommand();
        SqlDataReader dr;
        // GET: Akun
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        void ConnectionString()
        {
            con.ConnectionString = "data source=LAPTOP-OEHKD8AQ; database=Vaksin_Teknet; integrated security = SSPI;";
        }
        [HttpPost]
        public ActionResult Verify(Pasien pasien)
        {

            ConnectionString();
            con.Open();
            com.Connection = con;
            com.CommandText = "SELECT * FROM Pasien WHERE username='" + pasien.Username + "' AND password ='" + pasien.Password + "'";
            dr = com.ExecuteReader();
            if (dr.Read())
            {
                con.Close();

                return View("Berhasil");
            }
            else
            {
                con.Close();

                return View("Error");
            }
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RegisterPasien(Register registerDetails)
        {
            if (ModelState.IsValid)
            {
                using (var databaseContext = new Vaksin_TeknetEntities())
                {



                    Pasien reglog = new Pasien();

                    reglog.Pasien_ID = registerDetails.Pasien_ID;
                    reglog.Nama_Pasien = registerDetails.Nama_Pasien;
                    reglog.No_Hp = registerDetails.No_Hp;
                    reglog.Alamat = registerDetails.Alamat;
                    reglog.Username = registerDetails.Username;
                    reglog.Gejala_Pasien = registerDetails.Gejala_Pasien;
                    reglog.Password = registerDetails.Password;

                    databaseContext.Pasiens.Add(reglog);
                
                    databaseContext.SaveChanges();

                }

                ViewBag.Message = "User Details Saved";
                return View("Register");
            }
            else
            {

                return View("Register", registerDetails);
            }
        }
    }
}